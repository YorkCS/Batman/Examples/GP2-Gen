# GP2 Gen

Here lies an example implementation of the GP2 graph generation service interface. In this example, we generate linked lists, for use for benchmarking the sorting algorithms in the neighbouring example project.

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

You can start GP2 Gen using Docker.

```bash
$ docker run -d -p 9111:9111 registry.gitlab.com/yorkcs/batman/examples/gp2-gen
```

**NB Usually this software will be run by Docker Compose (see the neighbouring example project).**

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/examples/gp2-gen:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/examples/gp2-gen
```

**NB You do not have to build this project to use it. Pre-built versions are already pushed to the registry. This merely documents that process.**
