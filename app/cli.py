from gen import GeneratorFactory
from sys import argv

class CLI(object):

    @staticmethod
    def run(m, n):
        graph, nodes, edges = GeneratorFactory.make(m).generate(n)
        print(graph)
        print((nodes, edges))


def main():
    CLI.run(argv[1], int(argv[2]))


if __name__ == '__main__':
    main()
