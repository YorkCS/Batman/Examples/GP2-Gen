class Node(object):

    def __init__(self, nodeid, label=None, color=None, rooted=False):
        self._nodeid = nodeid
        self._label = label
        self._color = color
        self._rooted = rooted

    def __str__(self):
        return '(' + str(self._nodeid) + ('(R)' if self._rooted else '') + \
                ', ' + str(self._label if self._label is not None else 'empty') + ((' # ' + self._color) if self._color else '') + ')'


class Edge(object):

    def __init__(self, edgeid, node1, node2, label=None, dashed=None, bidir=False):
        self._edgeid = edgeid
        self._node1 = node1
        self._node2 = node2
        self._label = label
        self._dashed = dashed
        self._bidir = bidir

    def source(self):
        return self._node1

    def target(self):
        return self._node2

    def adj_nodes(self):
        return (self.source(), self.target())

    def __str__(self):
        return '(' + str(self._edgeid) + ('(B)' if self._bidir else '') + \
                ', ' + str(self._node1) + ', ' + str(self._node2)  + \
                ', ' + str(self._label if self._label is not None else 'empty') + (' # dashed' if self._dashed else '') + ')'


class Graph(object):

    def __init__(self):
        self._nodes = []
        self._edges = []

    def add_node(self, label=None, color=None, rooted=False):
        self._nodes.append(
            Node(len(self._nodes), label=label, color=color, rooted=rooted)
        )

    def add_edge(self, node1, node2, label=None, dashed=False, bidir=False):

        if (node1 >= len(self._nodes)) or (node2 >= len(self._nodes)):
            raise ValueError('No edges to accommodate nodes ' + str(node1) + ' and ' + str(node2))

        self._edges.append(
            Edge(len(self._edges), node1, node2, label=label, dashed=dashed, bidir=bidir)
        )

    def degree(self, node):
        return len([i for i in self._edges if node in i.adj_nodes()])

    def __str__(self):
        return '[ ' + ' '.join([str(n) for n in self._nodes]) + ' | ' \
                     + ' '.join([str(e) for e in self._edges]) + ' ]\n\n'
