import abc
import random
from graph import *


class GraphGenerator(abc.ABC):

    @abc.abstractmethod
    def generate(self, n):
        pass


class ListGenerator(GraphGenerator):

    def generate(self, n):
        graph = Graph()

        if n < 1:
            return str(graph), 0, 0

        graph.add_node(label=str(random.randint(0, n-1)))

        for i in range(n-1):
            graph.add_node(label=str(random.randint(0, n-1)))
            graph.add_edge(i, i+1)

        return str(graph), n, n - 1


class GeneratorFactory(object):

    @staticmethod
    def make(mode):
        if mode == 'list':
            return ListGenerator()
        else:
            raise ValueError('Unrecognized graph generator mode.')
