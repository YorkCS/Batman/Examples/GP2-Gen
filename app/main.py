from grpc import insecure_channel, server, Server, StatusCode
from concurrent.futures import ThreadPoolExecutor
from traceback import print_exc
from time import sleep
import sys
import gc

from gen_pb2 import *
from gen_pb2_grpc import *

from gen import GeneratorFactory

class Service(GenServicer):

    def Generate(self, request, context):
        for i in range(request.range.min, request.range.max + 1, request.range.step):
            out = self._generate(request.mode, i, context)

            if out is None:
                break

            yield Graph(graph=out[0], nodes=out[1], edges=out[2])

        gc.collect()

    def _generate(self, m, n, c):
        try:
            return GeneratorFactory.make(m).generate(n)
        except BaseException as e:
            print(e)
            print_exc()
            sys.stdout.flush()
            sys.stderr.flush()
            c.abort(StatusCode.INTERNAL, 'Unable to generate graph!')


class Factory(object):

    @staticmethod
    def make(service = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        s = server(ThreadPoolExecutor(max_workers=16), options=GRPC_CHANNEL_OPTIONS)
        add_GenServicer_to_server(service or Service(), s)
        s.add_insecure_port('[::]:9111')

        return s


def main():
    s = Factory.make()
    s.start()

    try:
        while True:
            sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        s.stop(0)


if __name__ == '__main__':
    main()
